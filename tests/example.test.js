const puppeteer = require('puppeteer')
const puppeteerFirefox = require('puppeteer-firefox')
const puppeteerCore = require('puppeteer-core')

describe('My First Puppeteer Test', () => {
    it('should launch the browser', async function() {
        const browser = await puppeteerCore.launch({
            headless: false,
            // "executablePath": '/usr/bin/google-chrome-stable',
            // "executablePath": '/usr/bin/firefox',
            "executablePath": '/usr/bin/microsoft-edge-dev',
            // "executablePath": '/Applications/Google Chrome.app/Contents/MacOS/Google Chrome', // MacOS specific path
            // "executablePath": '/Applications/Microsoft Edge.app/Contents/MacOS/Microsoft Edge', // MacOS specific path
        })
        const page = await browser.newPage()
        await page.goto('http://example.com/')
        await page.waitForSelector('h1')
        await browser.close()
    })
})