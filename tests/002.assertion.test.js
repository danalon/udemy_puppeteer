const puppeteer = require('puppeteer')
const puppeteerFirefox = require('puppeteer-firefox')
const puppeteerCore = require('puppeteer-core')

const expect = require('chai').expect

const { click, getText, getCount, typeText, waitForText, shouldNotExist } = require('../lib/helpers')

describe('My Second Puppeteer Test', () => {
    let browser
    let page

    before(async function() {
        browser = await puppeteer.launch({
            headless: false,
            slowMo: 0,
            devtools: false,
            args: [
                '--start-fullscreen'
            ],
            // "executablePath": '/usr/bin/google-chrome-stable',
            // "executablePath": '/usr/bin/microsoft-edge-dev',
            // "executablePath": '/Applications/Google Chrome.app/Contents/MacOS/Google Chrome', // MacOS specific path
            // "executablePath": '/Applications/Microsoft Edge.app/Contents/MacOS/Microsoft Edge', // MacOS specific path
        })

        page = await browser.newPage()
        await page.setViewport({
            width: 1920,
            height: 1080
        })
        await page.setDefaultNavigationTimeout(5000)
        await page.setDefaultTimeout(5000)

        await page.goto('http://example.com/')
    })

    after(async function() {
        await browser.close()
    })

    it('should launch the browser', async function() {
        const title = await page.title()
        const url = await page.url()

        // const text = await page.$eval('h1', element => element.textContent)
        const text = await getText(page, 'h1')

        // const count = await page.$$eval('p', element => element.length)
        const count = await getCount(page, 'p')

        expect(title).to.be.a('string').that.to.equal('Example Domain')
        expect(url).to.include('example.com')
        expect(text).to.be.a('string').that.to.equal('Example Domain')
        expect(count).to.equal(2)



        await page.goto('http://zero.webappsecurity.com/')

        // await page.waitForSelector('#signin_button')
        // await page.click('#signin_button')
        await click(page, '#signin_button')

        // await page.waitForSelector(() => !document.querySelector('#signin_button'))
        // await page.waitForSelector('#signin_button', {
        //     hidden:true,
        //     timeout: 100000
        // })
        await shouldNotExist(page, '#signin_button')


        await page.waitForTimeout(3000)
    })
})