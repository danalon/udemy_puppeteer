const puppeteer = require('puppeteer')
const puppeteerFirefox = require('puppeteer-firefox')
const puppeteerCore = require('puppeteer-core')

const expect = require('chai').expect

const { click, getText, getCount, typeText, waitForText, shouldNotExist } = require('../../lib/helpers')

describe('Feedback Test', () => {
    let browser
    let page

    before(async function() {
        browser = await puppeteer.launch({
            headless: true,
            slowMo: 0,
            devtools: false,
            args: [
                '--start-fullscreen',
                // '--incognito'
            ],
            // "executablePath": '/usr/bin/google-chrome-stable',
            // "executablePath": '/usr/bin/microsoft-edge-dev',
            // "executablePath": '/Applications/Google Chrome.app/Contents/MacOS/Google Chrome', // MacOS specific path
            // "executablePath": '/Applications/Microsoft Edge.app/Contents/MacOS/Microsoft Edge', // MacOS specific path
        })
        /**
        // bellow two lines are alternative for opening an incognito mode
        const context = await browser.createIncognitoBrowserContext()
        page = await context.newPage()
        **/
        page = await browser.newPage()
        await page.setViewport({
            width: 1920,
            height: 1080
        })
        await page.setDefaultNavigationTimeout(5000)
        await page.setDefaultTimeout(5000)
    })

    after(async function() {
        await browser.close()
    })

    it('Display Feedback Form', async function() {
        await page.goto('http://zero.webappsecurity.com/')
        await click(page, '#feedback > div > strong')
        await typeText(page, '#name', 'Name')
        await typeText(page, '#email', 'example@gmail.com')
        await typeText(page, '#subject', 'Subject')
        await typeText(page, '#comment', 'Message into the text area')
    })

    it('Submit Feedback Form', async function() {
        await click(page, 'input[type="submit"]')

        const url = await page.url()
        expect(url).include('eedback.html')
    })
})