const puppeteer = require('puppeteer')
const puppeteerFirefox = require('puppeteer-firefox')
const puppeteerCore = require('puppeteer-core')

const { click, getText, getCount, typeText, waitForText, shouldNotExist } = require('../../lib/helpers')

describe('Login Test', () => {
    let browser
    let page

    before(async function() {
        browser = await puppeteer.launch({
            headless: false,
            slowMo: 0,
            devtools: false,
            args: [
                '--start-fullscreen',
                // '--incognito'
            ],
            // "executablePath": '/usr/bin/google-chrome-stable',
            // "executablePath": '/usr/bin/microsoft-edge-dev',
            // "executablePath": '/Applications/Google Chrome.app/Contents/MacOS/Google Chrome', // MacOS specific path
            // "executablePath": '/Applications/Microsoft Edge.app/Contents/MacOS/Microsoft Edge', // MacOS specific path
        })
        /**
        // bellow two lines are alternative for opening an incognito mode
        const context = await browser.createIncognitoBrowserContext()
        page = await context.newPage()
        **/
        page = await browser.newPage()
        await page.setViewport({
            width: 1920,
            height: 1080
        })
        await page.setDefaultNavigationTimeout(5000)
        await page.setDefaultTimeout(5000)
    })

    after(async function() {
        await browser.close()
    })

    it('Login test : Invalid Credentials', async function() {
        await page.goto('http://zero.webappsecurity.com/')

        // await page.waitForSelector('#signin_button')
        // await page.waitForSelector('#signin_button')
        await click(page, '#signin_button')
        await typeText(page, '#user_login', 'invalid')
        await typeText(page, '#user_password', 'invalid')
        await click(page, '#user_remember_me')
        await click(page, '#login_form > div.form-actions > input')
        await page.waitForSelector('#login_form > div.alert.alert-error')
    })

    it('Login test : Valid Credentials', async function() {
        await page.goto('http://zero.webappsecurity.com/')

        // await page.waitForSelector('#signin_button')
        // await page.waitForSelector('#signin_button')
        await click(page, '#signin_button')
        await typeText(page, '#user_login', 'username')
        await typeText(page, '#user_password', 'password')
        await click(page, '#user_remember_me')
        await click(page, '#login_form > div.form-actions > input')

        await click(page, '#details-button')
        await click(page, '#proceed-link')

        await page.waitForSelector('#account_summary_tab')
    })
})