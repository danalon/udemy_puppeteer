const puppeteer = require('puppeteer')
const puppeteerFirefox = require('puppeteer-firefox')
const puppeteerCore = require('puppeteer-core')

const expect = require('chai').expect

const { click, getText, getCount, typeText, waitForText, shouldNotExist, selectOption } = require('../../lib/helpers')

describe('Payment Test', () => {
    let browser
    let page

    before(async function() {
        browser = await puppeteer.launch({
            headless: false,
            slowMo: 0,
            devtools: false,
            args: [
                '--start-fullscreen',
                // '--incognito'
            ],
            // "executablePath": '/usr/bin/google-chrome-stable',
            // "executablePath": '/usr/bin/microsoft-edge-dev',
            // "executablePath": '/Applications/Google Chrome.app/Contents/MacOS/Google Chrome', // MacOS specific path
            // "executablePath": '/Applications/Microsoft Edge.app/Contents/MacOS/Microsoft Edge', // MacOS specific path
        })
        /**
         // bellow two lines are alternative for opening an incognito mode
         const context = await browser.createIncognitoBrowserContext()
         page = await context.newPage()
         **/
        page = await browser.newPage()
        await page.setViewport({
            width: 1920,
            height: 1080
        })
        await page.setDefaultNavigationTimeout(5000)
        await page.setDefaultTimeout(5000)

        await page.goto('http://zero.webappsecurity.com/login.html')
        await page.waitForSelector('#login_form')

        await typeText(page, '#user_login', 'username')
        await typeText(page, '#user_password', 'password')
        await click(page, '#user_remember_me')
        await click(page, '#login_form > div.form-actions > input')

        await click(page, '#details-button')
        await click(page, '#proceed-link')

        await page.waitForSelector('#account_summary_tab')
    })

    after(async function() {
        await browser.close()
    })

    it('Display Payment Form', async function() {
        await click(page, '#pay_bills_tab')
        await page.waitForSelector('.board')
    })

    it('Make Payment', async function() {
        await selectOption(page, '#sp_payee', 'apple')
        await selectOption(page, '#sp_account', '4')
        await typeText(page, '#sp_amount', '100')
        await typeText(page, '#sp_date', '2021-02-17')
        await page.keyboard.press('Enter')

        await typeText(page, '#sp_description', 'description')
        await page.waitForTimeout(3000)
        await click(page, '#pay_saved_payees')
        await page.waitForSelector('#alert_content')
    })
})