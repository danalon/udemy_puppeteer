const puppeteer = require('puppeteer')
const puppeteerFirefox = require('puppeteer-firefox')
const puppeteerCore = require('puppeteer-core')

const expect = require('chai').expect

describe('My Third Puppeteer Test', () => {
    let browser
    let page

    before(async function() {
        browser = await puppeteer.launch({
            headless: false,
            slowMo: 0,
            devtools: false,
            args: [
                '--start-fullscreen'
            ],
            // "executablePath": '/usr/bin/google-chrome-stable',
            // "executablePath": '/usr/bin/microsoft-edge-dev',
            // "executablePath": '/Applications/Google Chrome.app/Contents/MacOS/Google Chrome', // MacOS specific path
            // "executablePath": '/Applications/Microsoft Edge.app/Contents/MacOS/Microsoft Edge', // MacOS specific path
        })
        const context = await browser.createIncognitoBrowserContext()

        page = await context.newPage()
        await page.setViewport({
            width: 1920,
            height: 1080
        })

        await page.goto('http://example.com/')
    })

    after(async function() {
        await browser.close()
    })

    it('Desktop device test', async function() {
        await page.setViewport({ width: 1650, height: 1050 })
        await page.goto('http://example.com/')
        await page.waitForTimeout(3000)
    })

    it('Tablet device test', async function() {
        const tablet = puppeteer.devices['iPad landscape']
        await page.emulate(tablet)
        await page.goto('http://example.com/')
        await page.waitForTimeout(3000)
    })

    it('Mobile device test', async function() {
        const mobile = puppeteer.devices['iPhone X']
        await page.emulate(mobile)
        await page.goto('http://example.com/')
        await page.waitForTimeout(3000)
    })
})